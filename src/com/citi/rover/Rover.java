package src.com.citi.rover;

public class Rover{

    private int X = 0;
    private int Y = 0;
    // public int stepsToGo = 2;
    private int direction;
    private StringBuilder nextSteps = new StringBuilder("4R2R1L2");

    public void receiveCommands(String commands){
        System.out.println("receiveCommands has been called");
    }

    public void takeNextStep(){
        System.out.println("takeNextStep has been called");
        Y += 1;
        // stepsToGo = stepsToGo - 1;
        if (nextSteps.length() > 0){
            switch(nextSteps.substring(0, 1)){
                case "L":
                this.direction -= 1;
                // nextSteps.deleteCharAt(1);
                case "R": 
                this.direction += 1;
                // nextSteps.deleteCharAt(1);

                default:
                switch(this.direction){
                    
                    case 0:
                    this.Y += 1;
                    case 1:
                    this.X += 1;
                    case 2:
                    this.Y -= 1;
                    case 3:
                    this.X -= 1;
                }
            }
            nextSteps.deleteCharAt(1);
            

            
        }
        else{
            System.out.println("x");
        }
        
    }

    public int getDirection(){
        System.out.println("getDirection has been called");
        return this.direction;
    }

    public boolean isBusy(){
        if (nextSteps.length() > 0){
            return true;
        }
        return false;
    }

    public int getX() {
        return X;
    }

    public void setX(int x) {
        X = x;
    }

    public int getY() {
        return Y;
    }

    public void setY(int y) {
        Y = y;
    }

}